FROM python:3.10-alpine

ENV PIP_DISABLE_PIP_VERSION_CHECK True

COPY requirements.txt /var/lib/setup/requirements.txt

RUN pip3 install -r /var/lib/setup/requirements.txt

VOLUME /data

WORKDIR /data

ENTRYPOINT ["cookiecutter"]
