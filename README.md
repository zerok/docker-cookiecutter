# Cookiecutter Docker image

This is a simple docker image for the cookiecutter project.

## How to use

More or less just mount whatever directories you need in your container (e.g. the template directory and the folder you want to create the new project in) 🙂

```sh
docker run --rm -i -t \
  -v $PWD:/data \
  -v /path/to/template:/src \
  registry.gitlab.com/zerok/docker-cookiecutter /src
```

## Maintenance

This repository is automatically updated unless the automation on my end somehow fails 😅
